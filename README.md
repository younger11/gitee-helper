# gitee-helper

> gitee 通知客户端，使用 electron-vue template 创建的 electron 项目

#### 功能
- [x] 使用私人令牌登录
- [x] 可浏览 @我、通知、私信
- [x] 动态通知 
- [x] 状态栏图标
- [x] 通知开关
- [x] 系统通知
- [x] 设置页功能

#### 编译步骤
> 本地进行服务端调试时可复制 .env 为 .env.local，并修改 host 为本地服务器地址

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:9081
npm run dev

# build electron application for production
npm run build

# run unit & end-to-end tests
npm test


# lint all JS/Vue component files in `src/`
npm run lint

```

#### 截图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/235053_07ae9538_409700.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/235041_224d9f2b_409700.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/235016_40b86784_409700.png "屏幕截图.png")


---

This project was generated with [electron-vue](https://github.com/SimulatedGREG/electron-vue)@[8fae476](https://github.com/SimulatedGREG/electron-vue/tree/8fae4763e9d225d3691b627e83b9e09b56f6c935) using [vue-cli](https://github.com/vuejs/vue-cli). Documentation about the original structure can be found [here](https://simulatedgreg.gitbooks.io/electron-vue/content/index.html).

#### 感谢
前期第一版开发主要依赖原作者 @lei2jun 的架子搭建，https://gitee.com/lei2jun/gitee-helper  
我是基于第一版进行了若干调整，主要增加了使用 Gitee 私人令牌进行登录的方式，以及增加设置页和退出登录的功能。  