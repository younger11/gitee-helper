'use strict'

// import '../renderer/store'
import { app, BrowserWindow, Menu, ipcMain, Tray } from 'electron'
import Positioner from 'electron-positioner'
import path from 'path'

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
let positioner
let willQuitApp = false
let tray = null
let trayIconPath = path.join(__dirname, '../../build/icons/16x16.png')
let trayGreyIconPath = path.join(__dirname, '../../build/icons/tray-grey.png')

const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9081`
  : `file://${__dirname}/index.html`

const appMenuTemplate = [
  {
    label: '编辑',
    submenu: [
      { role: 'cut', label: '剪切' },
      { role: 'copy', label: '复制' },
      { role: 'paste', label: '粘贴' },
      { role: 'selectAll', label: '全选' }
    ]
  }
]

if (process.platform === 'darwin') {
  appMenuTemplate.unshift({
    label: app.getName(),
    submenu: [
      {
        label: '切换登陆',
        click () {
          mainWindow.webContents.send('logout')
        }
      },
      {
        label: '退出',
        accelerator: 'CmdOrCtrl+Q',
        click () {
          mainWindow.destroy()
        }
      }
    ]
  })
}

let isMute = false

let menuTemplate = [
  {
    label: '打开主面板',
    click () {
      showMainWindow(mainWindow)
    }
  },
  {
    id: 'notify-mute',
    label: '取消静音',
    click () {
      isMute = !isMute

      if (isMute) {
        mainWindow.webContents.send('mute', true)
      } else {
        mainWindow.webContents.send('mute', false)
      }
    }
  },
  {
    label: '退出',
    click () {
      mainWindow.destroy()
    }
  }
]

const menu = Menu.buildFromTemplate(menuTemplate)
const appMenu = Menu.buildFromTemplate(appMenuTemplate)

function showMainWindow (mainWindow) {
  if (mainWindow) {
    if (mainWindow.isVisible()) {
      if (mainWindow.isFocused()) {
        mainWindow.hide()
      } else {
        mainWindow.show()
      }
    } else {
      mainWindow.show()
    }
  }
}

function setTrayIcon (status) {
  const item = menu.getMenuItemById('notify-mute')

  if (status === 'notify') {
    item.label = '静音'
    tray.setImage(trayIconPath)
  } else if (status === 'no-notify') {
    item.label = '取消静音'
    tray.setImage(trayGreyIconPath)
  }
  tray.setContextMenu(Menu.buildFromTemplate(
    menu.items.map(item => ({ label: item.label, id: item.id, click: item.click }))
  ))
}

function createWindow () {
  Menu.setApplicationMenu(appMenu)

  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    frame: false,
    height: 400,
    minHeight: 400,
    maxHeight: 800,
    width: 500,
    minWidth: 500,
    maxWidth: 800,
    useContentSize: true,
    resizable: false,
    webPreferences: { webSecurity: false }
  })
  positioner = new Positioner(mainWindow)

  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  mainWindow.on('close', (e) => {
    if (!willQuitApp) {
      e.preventDefault()
      mainWindow.hide()
    }
  })

  tray = new Tray(trayIconPath)
  tray.setToolTip('Gitee Helper')
  tray.setContextMenu(menu)
  tray.on('click', () => {
    showMainWindow(mainWindow)
  })
}

ipcMain.on('open-window', () => {
  mainWindow.show()
})

ipcMain.on('set-size', (e, { width = 500, height = 400 } = {}) => {
  mainWindow.setSize(width, height)
})

ipcMain.on('set-position', (e, place = 'center') => {
  positioner.move(place)
})

ipcMain.on('quit', () => {
  mainWindow.destroy()
})

ipcMain.on('hide', (e) => {
  mainWindow.hide()
})

ipcMain.on('minimize', (e) => {
  mainWindow.minimize()
})

ipcMain.on('set-tray-icon', (e, status) => {
  setTrayIcon(status)
})

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  app.quit()
})

app.on('activate', () => {
  mainWindow && mainWindow.show()
  if (mainWindow === null) {
    createWindow()
  }
})

app.on('before-quit', (e) => {
  willQuitApp = true
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
