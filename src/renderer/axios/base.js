import getInstance from './instance'
import {
  Message
} from 'element-ui'

const instance = getInstance()

// http request 拦截器
instance.interceptors.request.use(
  config => {
    return config
  },
  err => {
    return Promise.reject(err)
  })

instance.interceptors.response.use(
  res => {
    return Promise.resolve(res.data)
  },
  err => {
    try {
      if ((err + '').search('timeout') !== -1) {
        Message.error('请求超时')
      }
      if (err.response && err.response.data && err.response.data.error) {
        Message.error(err.response.data.error_description || err.response.data.error)
      } else {
        console.error(err)
      }
    } catch (err) {
      console.error(err)
    }
    return Promise.reject(err)
  }
)

export default instance
