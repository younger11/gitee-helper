import getInstance from './instance'
import {
  Message
} from 'element-ui'
const instance = getInstance()

// http request 拦截器
instance.interceptors.request.use(
  config => {
    return config
  },
  err => {
    return Promise.reject(err)
  }
)

instance.interceptors.response.use(
  res => {
    return res.data
  },
  err => {
    if (err.response && err.response.status === 401) {
      Message.error(err.response.data.message)
    }
    return Promise.reject(err.response)
  }
)

export default instance
