import instance from './base'

export default (url, data, options) => {
  return instance(Object.assign({
    url: url,
    method: 'delete',
    data: data
  }, options))
}
