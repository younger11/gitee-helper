import instance from './base'
import instanceSimple from './baseSimple'

let get = (url, params, options) => {
  return instance(Object.assign({
    url: url,
    method: 'get',
    params: params
  }, options))
}

get.basic = (url, params, options) => {
  return instanceSimple(Object.assign({
    url: url,
    method: 'get',
    params: params
  }, options))
}

export default get
