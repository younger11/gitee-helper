import Get from './get'
import Post from './post'
import Put from './put'
import Delete from './delete'
import Patch from './patch'
import Base from './base'

export {
  Get,
  Post,
  Put,
  Delete,
  Patch,
  Base
}
