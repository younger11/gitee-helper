import axios from 'axios'
import Qs from 'qs'

const getInstance = () => {
  const instance = axios.create()
  instance.defaults.withCredentials = true
  instance.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
  instance.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded;charset-utf-8'
  instance.defaults.paramsSerializer = (params) => {
    return Qs.stringify(params, {arrayFormat: 'brackets'})
  }
  return instance
}

export default getInstance
