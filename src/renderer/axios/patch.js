import instance from './base'
import Qs from 'qs'

export default (url, data, options) => {
  return instance(Object.assign({
    url: url,
    method: 'patch',
    data: data,
    transformRequest: [function (data, headers) {
      return Qs.stringify(data)
    }]
  }, options))
}
