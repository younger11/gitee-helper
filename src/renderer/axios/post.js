import instance from './base'
import instanceSimple from './baseSimple'

let post = (url, data, options) => {
  return instance(Object.assign({
    url: url,
    method: 'post',
    data: data
  }, options))
}

post.basic = (url, data, options) => {
  return instanceSimple(Object.assign({
    url: url,
    method: 'post',
    data: data
  }, options))
}

export default post
