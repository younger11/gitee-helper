const yaml = require('js-yaml')
const deepmerge = require('deepmerge')
const fs = require('fs')
const path = require('path')
let config = {}
let prodConfig = {}
let devConfig = {}

try {
  prodConfig = yaml.safeLoad(fs.readFileSync(path.join(__dirname, '../../.env'), 'utf8'))
} catch (e) {
  prodConfig = {}
}

try {
  devConfig = yaml.safeLoad(fs.readFileSync(path.join(__dirname, '../../.env.local'), 'utf8'))
} catch (e) {
  devConfig = {}
}

if (process.env.NODE_ENV === 'production') {
  config = prodConfig
} else {
  config = deepmerge(prodConfig, devConfig)
}

export default config
