import Vue from 'vue'
import axios from 'axios'
import ElementUI from 'element-ui'
import VueTimeago from 'vue-timeago'
import VueScrollTo from 'vue-scrollto'
import Vuebar from 'vue-scrollar'
import { ipcRenderer } from 'electron'
import { linkFilter } from '@/utils/filters'
import 'normalize.css/normalize.css'
import 'element-ui/lib/theme-chalk/index.css'

import toploadingDirective from '@/directives/top-loading'
import scrollShadowDirective from '@/directives/scroll-shadow'
import OpenBrowser from '@/components/OpenBrowser'
import App from './App'
import router from './router'
import store from './store'
import '@style/app.scss'

import ShareDataPlugin from './plugins/share-data'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.use(ElementUI, { size: 'small', zIndex: 3000 })
Vue.use(VueTimeago, {
  name: 'Timeago',
  locale: 'zh-CN',
  locales: {
    'zh-CN': require('date-fns/locale/zh_cn'),
    'en': require('date-fns/locale/en')
  }
})
Vue.use(VueScrollTo)
Vue.use(toploadingDirective)
Vue.use(scrollShadowDirective)
Vue.use(Vuebar)
Vue.use(ShareDataPlugin)

Vue.component('Open-browser', OpenBrowser)

Vue.filter('linkFilter', linkFilter)

Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')

ipcRenderer.send(
  'set-tray-icon',
  store.state.notify.notifyStatus ? 'notify' : 'no-notify'
)
