const notifyBus = () => {}
const updaters = Object.create(null)

/**
 * 监听 provider 值变化
 * @param {String} key 分割作用域的唯一 ID
 * @param {Function} updater 用来监听变化的回调
 */
notifyBus.on = (key, name, updater) => {
  if (key && name && updater instanceof Function) {
    updaters[key] = updaters[key] || {}
    updaters[key][name] = (updaters[key][name] || []).concat(updater)
  }
}

/**
 * 删除 provider 监听
 * @param {String} key 分割作用域的唯一 ID
 */
notifyBus.off = (key, name, updater) => {
  let index = -1
  if (key && name && updaters[key] && updaters[key][name]) {
    index = updaters[key][name].indexOf(updater)
    if (index > -1) {
      updaters[key][name].splice(index, 1)
    }
  }
}

/**
 * 用来触发 on 方法绑定的回调
 * @param {String} key 分割作用域的唯一 ID
 * @param {Function} payload 向回调传递的参数
 */
notifyBus.update = (key, name, ...payload) => {
  if (key && name && updaters[key] && updaters[key][name]) {
    updaters[key][name].forEach(u => u(...payload))
  }
}

export default notifyBus
