import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

import { ipcRenderer } from 'electron'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      redirect: { name: 'atme' }
    },
    {
      path: '/login',
      name: 'login',
      component: require('@/views/Login').default
    },
    {
      path: '/notifications',
      name: 'notifications',
      redirect: { name: 'atme' },
      component: require('@/views/Notifications').default,
      children: [
        {
          path: 'atme',
          name: 'atme',
          component: require('@/components/notification/AtMe').default
        },
        {
          path: 'user_message',
          name: 'userMessage',
          component: require('@/components/notification/UserMessage').default
        },
        {
          path: 'event',
          name: 'event',
          component: require('@/components/notification/Event').default
        },
        {
          path: 'setting',
          name: 'setting',
          component: require('@/components/notification/Setting').default
        },
        {
          path: 'about',
          name: 'about',
          component: require('@/components/notification/About').default
        },
        {
          path: 'issues',
          name: 'issues',
          component: require('@/views/Issues/Index').default
        },
        {
          path: 'issues/:owner/:repo/:number',
          name: 'issuesDetail',
          component: require('@/views/Issues/Detail').default
        }
      ]
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

router.beforeEach((to, from, next) => {
  const token = store.state.user.accessToken
  if (!token && to.name !== 'login') {
    return next({ name: 'login' })
  }
  if (to.name === 'login') {
    ipcRenderer.send('set-size', { width: 1000, height: 800 })
    ipcRenderer.send('set-position', 'center')
  } else if (from.name === 'login' || !from.name) {
    ipcRenderer.send('set-size', { width: 800, height: 700 })
    ipcRenderer.send('set-position', 'center')
  }

  next()
})

export default router
