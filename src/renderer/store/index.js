import Vue from 'vue'
import Vuex from 'vuex'
import rootStore from './root'

import createPersistedState from 'vuex-persistedstate'

import userModule from './modules/users'
import notifyModule from './modules/notifications/index'
import reposModule from './modules/repos'
import issuesModule from './modules/issues'

Vue.use(Vuex)

export default new Vuex.Store({
  ...rootStore,
  modules: {
    user: userModule,
    notify: notifyModule,
    repos: reposModule,
    issues: issuesModule
  },
  plugins: [
    createPersistedState({
      key: 'gitee-helper',
      paths: ['user', 'repos.currentRepo']
    })
  ],
  strict: process.env.NODE_ENV !== 'production'
})
