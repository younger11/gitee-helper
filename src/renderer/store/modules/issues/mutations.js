import types from './types'

export default {
  [types.SET_ISSUES] (state, issues) {
    if (issues instanceof Array) {
      state.issues = issues
    } else {
      console.error(`[mutations-issues:${types.SET_ISSUES}]->`, 'issues 格式不正确：', issues)
    }
  },

  [types.SET_CURRENT_REPO] (state, currentRepo) {
    state.currentRepo = currentRepo
  },

  [types.SET_PAGE_INFO] (state, pageInfo) {
    Object.assign(state, pageInfo)
  },

  [types.SET_CURRENT_ISSUE] (state, issue) {
    state.currentIssue = issue
  },

  [types.SET_CURRENT_COMMENTS] (state, comments) {
    if (!comments) {
      console.error(`[mutations-issues:${types.SET_CURRENT_COMMENTS}]->`, 'comments 格式不正确：', comments)
      return
    }
    state.currentComments = comments
  }
}
