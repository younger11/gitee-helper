export default {
  issues: [],
  currentRepo: '',
  page: 0,
  perPage: 20,
  issuesLoading: false,
  issuesLoaded: false,
  currentIssue: null,
  issueLoading: false,
  issueLoaded: false,
  currentComments: [],
  commentsLoading: false,
  commentsPage: 0,
  commentsPerPage: 20,
  commentsLoaded: false
}
