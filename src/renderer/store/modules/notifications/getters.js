export default {
  atmeNotice (state) {
    return state.notifications.atme
  },

  userMessage (state) {
    return state.notifications.userMessage
  },

  eventNotice (state) {
    return state.notifications.event
  },

  listPage (state) {
    return state.listPerPage
  }
}
