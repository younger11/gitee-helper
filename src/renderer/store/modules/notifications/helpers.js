import { showWindowNotify } from '@/utils'
export const checkWindowNofify = (state, type, data) => {
  // FIXME: 判断程序是否在后台，在后台才弹出系统通知
  if (state.notifyStatus && data.list.length > 0) {
    for (let item of data.list) {
      if (Date.parse(item.updated_at) < Date.parse(window.appDate)) {
        return
      }
      if (type === 'atme' && state.mentionMe === 1) {
        const option = {
          title: '有人 @ 你',
          body: item.content,
          href: item.html_url
        }
        showWindowNotify(option.title, option)
      } else if (type === 'userMessage' && state.privateMessage === 1) {
        const option = {
          title: '有新的私信',
          body: item.content,
          href: item.html_url_notice_dropdown
        }
        showWindowNotify(option.title, option)
      } else if (state.noticeMe === 1) {
        const option = {
          title: '新的消息动态',
          body: item.content,
          href: item.html_url
        }
        showWindowNotify(option.title, option)
      }
    }
  }
}
