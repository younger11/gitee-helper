import state from './state'
import getters from './getters'
import actions from './actions'
import mutations from './mutations'

// 初始化系统时间
window.appDate = new Date()

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
