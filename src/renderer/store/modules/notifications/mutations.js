import Vue from 'vue'
import types from './types'
import { checkWindowNofify } from './helpers.js'

export default {
  [types.SET_NOTIFICATIONS] (state, { type, data }) {
    data = Object.assign({ loading: false }, data)
    Vue.set(state.notifications, type, data)
    checkWindowNofify(state, type, data)
  },
  [types.SET_NOTICE_READ] (state, { id, type }) {
    state.notifications[type].list.some((notice, index) => {
      if (notice.id === id) {
        notice.unread = false
        return true
      }
    })
  },
  [types.SET_USER_MESSAGE_READ] (state, { id }) {
    state.notifications['userMessage'].list.some((notice, index) => {
      if (notice.id === id) {
        notice.unread = false
        return true
      }
    })
  },
  [types.SET_NOTICE_LOADING] (state, { type, value }) {
    state.notifications[type].loading = value
  },
  [types.SET_FIRST_PAGE] (state, { type, data }) {
    state.firstPages[type] = data
  },
  [types.SET_EVENT_FILTERED] (state, value) {
    state.eventFiltered = value
  },
  [types.SET_AT_ME_FILTERED] (state, value) {
    state.atmefiltered = value
  },
  [types.NOTIFY_STATUE_TOGGLE] (state) {
    state.notifyStatus = !state.notifyStatus
  },
  [types.SET_INTERVAL_FETCH_API] (state, value) {
    state.pollingTimer = value
  },
  [types.SET_SETTING_FORM] (state, data) {
    if (data.fetchApiTimes !== state.fetchApiTimes) {
      state.fetchApiTimes = data.fetchApiTimes
    }
    state.mentionMe = data.mentionMe
    state.noticeMe = data.noticeMe
    state.privateMessage = data.privateMessage
  }
}
