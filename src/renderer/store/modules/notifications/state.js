export default {
  notifyStatus: true,
  atmefilters: [
    { label: '未读', value: 'unread' },
    { label: '所有', value: 'all' }
  ],
  atmefiltered: 'unread',
  eventFilters: [
    { label: '未读', value: 'unread' },
    { label: '我参与的', value: 'participating' },
    { label: '所有', value: 'all' }
  ],
  eventFiltered: 'unread',
  notifications: {
    atme: {
      list: [],
      total_count: 0,
      current_page: 1,
      loading: false
    },
    event: {
      list: [],
      total_count: 0,
      current_page: 1,
      loading: false
    },
    userMessage: {
      list: [],
      total_count: 0,
      current_page: 1,
      loading: false
    }
  },
  listPerPage: 20,
  firstPages: {
    atme: {
      list: []
    },
    event: {
      list: []
    },
    userMessage: {
      list: []
    }
  },
  pollingTimer: null,
  fetchApiTimes: 10000,
  mentionMe: 1,
  noticeMe: 0,
  privateMessage: 1
}
