import types from './types'

export default {
  [types.SET_REPOS] (state, repos) {
    if (repos instanceof Array) {
      state.repos = repos
    } else {
      console.error('[mutations-repos]->', '仓库数据格式不正确：', repos)
    }
  },

  [types.SET_CURRENT_REPO] (state, currentRepo) {
    state.currentRepo = currentRepo
  },

  [types.SET_PAGE_INFO] (state, pageInfo) {
    Object.assign(state, pageInfo)
  }
}
