export default {
  repos: [],
  reposLoading: false,
  reposLoaded: true,
  page: 0,
  perPage: 20,
  currentRepo: ''
}
