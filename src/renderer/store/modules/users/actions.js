import types from './types'
import { login } from '@/api'
import router from '@/router'

const loginAction = async ({ commit }, form) => {
  const userInfo = await login(form)
  if (userInfo) {
    commit(types.SET_ACCESS_TOKEN, form.secretToken)
    commit(types.SET_USER_INFO, userInfo)
    router.push({ name: 'notifications' })
  }
}

const logoutAction = async ({ commit }) => {
  commit(types.LOGOUT)
}

export default {
  login: loginAction,
  logout: logoutAction
}
