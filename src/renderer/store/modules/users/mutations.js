import types from './types'

export default {
  [types.SET_USER_INFO] (state, userInfo) {
    state.userInfo = userInfo
  },

  [types.LOGOUT] (state) {
    state.userInfo = null
    state.accessToken = null
  },

  [types.SET_ACCESS_TOKEN] (state, accessToken) {
    state.accessToken = accessToken
  }
}
