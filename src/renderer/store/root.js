const types = {
  SET_CURRENT_TAB: 'SET_CURRENT_TAB'
}
const state = {
  currentTab: 'atme'
}

const mutations = {
  [types.SET_CURRENT_TAB] (state, tab) {
    state.currentTab = tab
  }
}

const actions = {
  setCurrentTab ({ commit }, tab) {
    commit(types.SET_CURRENT_TAB, tab)
  }
}

export default {
  state,
  mutations,
  actions
}
