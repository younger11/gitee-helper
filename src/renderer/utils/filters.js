export const linkFilter = (text) => {
  return text.replace(/<a\s.+?>(.+?)<\/a>/g, '$1')
}
