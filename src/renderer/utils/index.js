import { shell, ipcRenderer } from 'electron'
import store from '@/store'
import EmptyNotify from './empty-notify'

export const hasAvatar = (avatarURL) => {
  return !avatarURL.endsWith('no_portrait.png')
}

export const noticeNamespaceTypeText = (type) => {
  if (type === 'enterprise') {
    return '企业'
  } else if (type === 'group') {
    return '组织'
  } else if (type === 'code') {
    return '代码片段'
  } else if (type === 'project') {
    return '项目'
  }
}

export const getNoticeDiff = (originalData, data) => {
  let result = []
  data.list.some((newItem) => {
    if (originalData &&
      originalData.list &&
      newItem.id === originalData.list[0].id
    ) {
      return true
    }
    result.push(newItem)
  })
  return result
}

export const notify = (title = '', options) => {
  let notice
  if (store.state.notify.notifyStatus) {
    notice = new Notification(title, options)
  } else {
    notice = new EmptyNotify()
  }
  return notice
}

export const notifyNotification = (diff1, diff2, diff3) => {
  let notice
  if (diff1.length || diff2.length || diff3.length) {
    if (diff1.length + diff2.length + diff3.length === 1) {
      let diff = diff1[0] || diff2[0] || diff3[0]
      notice = notify('您有新的通知', {
        body: diff.content
      })
      notice.onclick = () => {
        shell.openExternal(diff.html_url)
      }
    } else {
      notice = notify('您有多条新的通知', {
        body: '您可以打开通知中心查看'
      })
      notice.onclick = () => {
        ipcRenderer.send('open-window')
      }
    }
  }
}

export const showWindowNotify = (title, option) => {
  const hhwNotication = new window.Notification(title, option)
  hhwNotication.onclick = function () {
    shell.openExternal(option.href)
  }
  window.appDate = new Date()
}

export const openInBrowser = (url) => {
  shell.openExternal(url)
}
