import { getNoticeDiff } from '@/utils'

describe('utils', () => {
  it('应得到差异的开始部分', () => {
    let res = getNoticeDiff({
      list: [
        {id: 3},
        {id: 4},
        {id: 5},
        {id: 6}
      ]
    }, {
      list: [
        {id: 1},
        {id: 2},
        {id: 3},
        {id: 4},
        {id: 5},
        {id: 6}
      ]
    })
    expect(JSON.stringify(res)).to.equal(JSON.stringify([{id: 1}, { id: 2 }]))
  })
})
